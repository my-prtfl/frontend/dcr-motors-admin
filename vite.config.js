import { fileURLToPath, URL } from 'url'

import path from "path"
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import svgLoader from "vite-svg-loader"

export default defineConfig({
  server: {
    port: 3001
  },
  plugins: [
    vue(),
    svgLoader()
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
      // '@': path.resolve(__dirname, './src')
    }
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `
          @import "bulma/sass/components/navbar.sass";
          @import "@/assets/scss/common/variables";
        `
      }
    }
  }
})
