FROM node:16-alpine
RUN apk update && apk upgrade && \
    apk add --no-cache git
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# FROM media-bot-crm:lates
ARG VITE_API_URI=${VITE_API_URI}
ENV VITE_API_URI=${VITE_API_URI}

COPY ./ /usr/src/app
RUN npm install && npm cache clean --force
RUN npm run build
ENV NODE_ENV production
ENV PORT 80
EXPOSE 80

CMD [ "npm", "start" ]