require("dotenv").config()

const express = require("express");
// const forceSsl = require("force-ssl-heroku");
const history = require("connect-history-api-fallback");
const path = require("path");
const serveStatic = require("serve-static");

const app = express();

app.use(
  history({
    verbose: true,
  })
);

app.use(serveStatic(path.join(__dirname, "/dist")));

const port = process.env.PORT || 5001;

app.listen(port, () => {
  console.log("Server started at http://localhost:" + port);
});