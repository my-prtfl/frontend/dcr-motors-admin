import ruDefault from "./ru/default"
import ruButtons from "./ru/buttons"
import ruForm from "./ru/form"
import enDefault from "./en/default"
import enButtons from "./en/buttons"
import enForm from "./en/form"

const data = {
	en: {
		default: enDefault,
		buttons: enButtons,
		form: enForm
	},
	ru: {
		default: ruDefault,
		buttons: ruButtons,
		form: ruForm
	}
}

export default data