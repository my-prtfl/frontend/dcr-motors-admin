export default {
	create: "Create",
	link: "Link",
	save: "Save",
	login: "Login",
	change: "Save",
	search: "Search",
	approve: "Confirm",
	retry: "Retry",
	resolve: "Resolve",
	cancel: "Cancel",
	markAsPaid: "Mark as paid",
	createPayment: "Create a surcharge",
	upload: "Загрузить",
	remove_selection: "Снять выделение",
	select: "Выбрать",
	select_all: "Выбрать всё",
	delete: "Удалить"
}