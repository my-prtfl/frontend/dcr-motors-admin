export default {
	create: "Создать",
	link: "Ссылка",
	save: "Сохранить",
	login: "Войти",
	change: "Изменить",
	search: "Поиск",
	approve: "Подтвердить",
	retry: "Повторить",
	resolve: "Подтвердить",
	cancel: "Отмена",
	markAsPaid: "Отметить как оплаченный",
	createPayment: "Создать доплату",
	upload: "Загрузить",
	remove_selection: "Снять выделение",
	select: "Выбрать",
	select_all: "Выбрать всё",
	delete: "Удалить"
}