import dayjs from "@/common/dayjs"

// Получаем максимально возможное значение для таймпикера начала аренды, если даты совпадают
export const getMax = (from, to) => {
	if (!from || !to) return "23:59"

	from = dayjs(from)
	to = dayjs(to)

	if (from.format("DD-MM-YYYY") === to.format("DD-MM-YYYY"))
		return to.subtract(1, "hour").format("HH:mm")

	return "23:59"
}

// Получаем минимально возможное значение для таймпикера конца аренды, если даты совпадают
export const getMin = (from, to) => {
	if (!from || !to) return "00:00"

	from = dayjs(from)
	to = dayjs(to)

	if (from.format("DD-MM-YYYY") === to.format("DD-MM-YYYY"))
		return from.add(1, "hour").format("HH:mm")

	return "00:00"
}

// Устанавливаем время для даты
export const setTime = (date, time) => {
	let hours = time.split(":")[0]
	let minutes = time.split(":")[1]
	
	return dayjs(date).set("hour", hours).set("minute", minutes).toDate()
}

// Устанавливаем дату начала аренды
export const setFrom = (date_from, date_to) => {
	let time = dayjs(date_from).format("HH:mm")
	let max = getMax(date_from, date_to)

	if (time > max)
		time = max
	
	return setTime(date_from, time)
}

// Устанавливаем дату окончания аренды
export const setTo = (date_from, date_to) => {
	let time = dayjs(date_to).format("HH:mm")
	let min = getMin(date_from, date_to)
	
	if (time < min)
		time = min
		
	return setTime(date_to, time)
}

export const toUtc = data => {
	const date = dayjs(data).format("YYYY-MM-DD")
	const time = dayjs(data).format("HH:mm:ss")

	return dayjs.utc(`${date}${time}`).format()
}

export const isValidDate = d => {
  return d instanceof Date && !isNaN(d);
}