import { defineAsyncComponent } from "vue"

export const Column = defineAsyncComponent(() => import("./Column.vue"))
export const EditForm = defineAsyncComponent(() => import("./Edit.vue"))