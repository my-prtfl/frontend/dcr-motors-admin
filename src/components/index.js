import { defineAsyncComponent } from "vue"

export const useComponents = (app) => {
	app.component("b-button", defineAsyncComponent(() => import(`../components/button/Button.vue`)))
	app.component("b-container", defineAsyncComponent(() => import(`../components/Container.vue`)))
	app.component("b-card", defineAsyncComponent(() => import(`../components/Card.vue`)))
	app.component("title-bar", defineAsyncComponent(() => import(`../components/TitleBar.vue`)))
	app.component("loading", defineAsyncComponent(() => import(`../components/Loading.vue`)))
}