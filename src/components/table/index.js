import { defineAsyncComponent } from "vue"

export const BTable = defineAsyncComponent(() => import("./Table.vue"))
export const TypesTable = defineAsyncComponent(() => import("./TypesTable.vue"))
export const BPagination = defineAsyncComponent(() => import("./Pagination.vue"))