import { defineAsyncComponent } from "vue"

export const BButton = defineAsyncComponent(() => import("./Button.vue"))