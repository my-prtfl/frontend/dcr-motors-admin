import { defineAsyncComponent } from "vue"

export const BModal = defineAsyncComponent(() => import("./Modal.vue")) 