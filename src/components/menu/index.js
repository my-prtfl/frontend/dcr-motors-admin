import { defineAsyncComponent } from "vue"

export const MenuWrapper = defineAsyncComponent(() => import("./MenuWrapper.vue"))
export const MenuList = defineAsyncComponent(() => import("./MenuList.vue"))
export const MenuItem = defineAsyncComponent(() => import("./MenuItem.vue"))