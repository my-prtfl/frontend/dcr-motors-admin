export const statusClass = {
	created: "is-success is-light",
	canceled: "is-danger",
	paid: "is-warning",
	expired: "is-danger is-light",
	inProgress: "is-info",
	ended: "is-success",
	confirmed: "is-info is-light",
	payError: "is-danger is-light"
}

export const tagClass = {
	created: "success",
	canceled: "danger",
	paid: "warning",
	expired: "danger",
	inProgress: "info",
	ended: "success",
	confirmed: "info",
	payError: "danger"
}

export const transactionTagClass = {
	created: {
		class: "success",
		light: true
	},
	confirmed: {
		class: "success"
	},
	delayed: {
		class: "danger",
		light: true
	},
	failed: {
		class: "danger"
	},
	pending: {
		class: "info"
	},
	unresolved: {
		class: "warning"
	},
	resolved: {
		class: "success"
	}
}