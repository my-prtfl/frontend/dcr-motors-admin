export default (name, bucket = "cars") => [ "full", 1920, 1280, 720, 480 ].reduce((a, v) => ({
	...a,
	[v]: `//${import.meta.env.VITE_API_URI}/${bucket}/${name}${v == "full" ? "" : "_" + v}.webp`
}), {})