import slugify from "slugify"

export default val => slugify(val, { locale: "en", lower: true, strict: true, replacement: "-" })