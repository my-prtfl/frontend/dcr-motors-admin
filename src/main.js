import "@/assets/scss/layout.scss"

import { createApp, defineAsyncComponent } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

const app = createApp(App)

// Директива для отслеживания клика вне элемента
import useClickOutside from "@/common/clickOutside"
useClickOutside(app)

// Уведомления
import Toast, { POSITION } from "vue-toastification"
app.use(Toast, { position: POSITION.TOP_RIGHT })

// Иконки
import "@/fontawesome/lib"

// Глобальные компоненты
import { useComponents } from "@/components"
useComponents(app)

// Переводы
import i18n from "@/i18n"
app.use(i18n)

app.use(createPinia())
app.use(router)

app.mount('#app')