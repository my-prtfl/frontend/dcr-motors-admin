import { defineStore } from "pinia"
import { Instance } from "@/api"

const api = new Instance("admin/settings")

export const useSettingsStore = defineStore("settings", {
	state: () => ({
		settings: {}
	}),

	getters: {
		get: state => name => state.settings[name]
	},

	actions: {
		async getSetting(name) {
			try {
				const setting = await api.get({ path: name })
				
				this.settings[name] = setting
			} catch(err) {
				console.error(err)
			}
		},
		async editSetting(name, body) {
			try {
				await api.put({ path: name, body })
				await this.getSetting(name)
			} catch(err) {
				console.error(err)
				return Promise.reject(err)
			}
		}
	}
})