import { defineStore } from "pinia"
import { Instance } from "@/api"

const api = new Instance("admin/images")

export const useImagesStore = defineStore("images", {
	state: () => ({
		images: null,
		pending: false
	}),
	getters: {},
	actions: {
		async getImages() {
			this.pending = true

			try {
				const { images } = await api.get()
				
				this.images = images
			} catch(err) {
				console.error(err)
			}

			this.pending = false
		},
		uploadImages(body) {
			return api.post({ body })
		},
		remove(body) {
			return api.put({ body })
		}
	}
})