import { defineStore } from "pinia"
import { Instance } from "@/api"
import query from "@/common/query"

const api = new Instance("admin/brands")

export const useBrandStore = defineStore("brand", {
	state: () => ({
		pending: false,
		brands: null,
		total: 0,
		page: 1,
		limit: 15,
		keyword: ""
	}),
	getters: {},
	actions: {
		async getBrands() {
			try {
				let path = query({
					page: this.page,
					limit: this.limit,
					keyword: this.keyword
				})

				
				this.pending = true
				const { brands, total } = await api.get({ path })
				this.brands = brands
				this.total = total
				this.pending = false
			} catch(err) {
				console.log(err)
			}
		},
		async getAllBrands() {
			try {
				this.pending = true
				const brands = await api.get({ path: "all" })
				this.pending = false

				return brands
			} catch(err) {
				console.error(err)
			}
		},
		getBrand(id) {
			return api.get({ path: id })
		},
		checkBrand(body) {
			return api.post({ path: "check", body })
		},
		async createBrand(body) {
			try {
				this.pending = true
				const data = await api.post({ body })
				this.pending = false
			} catch(err) {
				console.error(err)
			}
		},
		async editBrand(id, body) {
			try {
				this.pending = true
				await api.put({ path: id, body })
				this.pending = false
			} catch(err) {
				console.error(err)
			}
		},
		async deleteBrand(id) {
			try {
				this.pending = true
				await api.delete({ path: `/${id}` })
				this.pending = false
			} catch(err) {
				console.error(err)
			}
		}
	}
})