import { defineStore } from "pinia"
import query from "@/common/query"
import { Instance } from "@/api"

const api = new Instance("admin/clients")

export const useClientsStore = defineStore("clients", {
	state: () => ({
		clients: null,
		pending: false,
		total: 0,
		page: 1,
		limit: 100,
		keyword: ""
	}),
	getters: {},
	actions: {
		async getClients() {
			this.pending = true

			try {
				const params = {
					page: this.page,
					limit: this.limit,
					keyword: this.keyword
				}
	
				const { clients, total } = await api.get({ path: query(params) })
	
				this.clients = clients
				this.total = total
			} catch(err) {
				console.error(err)
			}

			this.pending = false
		},
		getClient(id) {
			return api.get({ path: id })	
		},
		updateClient(id, body) {
			return api.put({ path: id, body })
		},
		addClient(body) {
			return api.post({ body })
		}
	}
})