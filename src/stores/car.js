import { defineStore } from "pinia"
import { Instance } from "@/api"
import query from "@/common/query"

const api = new Instance("admin/cars")

const createBody = (data, update = false) => {
	const body = new FormData()

	const old_images = []
	for (let i in data.images) {
		let image = data.images[i]

		if (image.type) {
			let mimetype = image.name.split(".")[image.name.split(".").length - 1]
			body.append("files", image, `${i}.${mimetype}`)
		} else {
			image.position = parseInt(i)
			old_images.push(image)
		}
	}

	delete data.images
	if (update) {
		data.images = old_images
	}

	body.append("data", JSON.stringify(data))

	return body
}

export const useCarStore = defineStore("car", {
	state: () => ({
		pending: false,
		cars: null,
		total: 0,
		page: 1,
		limit: 100,
		keyword: ""
	}),
	actions: {
		async getCars() {
			try {
				this.pending = true

				let path = query({
					page: this.page,
					limit: this.limit,
					keyword: this.keyword
				})
	
				const { cars, total } = await api.get({ path })
				this.cars = cars
				this.total = total
			} catch(err) {
				console.error(err)
			}

			this.pending = false
		},
		async getCar(id) {
			return api.get({ path: id })
		},
		checkCar(body) {
			return api.post({ path: "check", body })
		},
		async editCar(id, data) {
			this.pending = true

			try {
				const body = createBody(data, true)

				await api.put({ path: id, body })
				this.pending = false
			} catch(err) {
				console.error(err)
				this.pending = false
				return Promise.reject(err)
			}
		},
		async createCar(data) {
			this.pending = true
			try {
				const body = createBody(data)

				await api.post({ body })
				this.pending = false
				return Promise.resolve()
			} catch(err) {
				console.error(err)
			}
			
			this.pending = false
			return Promise.reject()
		},
		async deleteCar(id) {
			try {
				this.pending = true
				await api.delete({ path: `/${id}` })
				await this.getCars()
			} catch(err) {
				console.error(err)
			}
		},
		getDates(id, orderID) {
			return api.get({ path: `dates${query({ id, orderID })}` })
		}
	}
})