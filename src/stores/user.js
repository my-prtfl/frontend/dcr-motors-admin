import { defineStore } from 'pinia'
import { Instance } from "@/api"

const api = new Instance("admin/auth")
const uapi = new Instance("admin/users")

export const useUserStore = defineStore('user', {
  state: () => ({
    user: null,
    users: null,
    pending: false
  }),
  actions: {
    async authByToken() {
      try {
        const user = await api.get()
        this.user = user

        localStorage.setItem("authorized", true)

        return true
      } catch (err) {
        console.error(err)
      }
    },
    async authByCredentials(body) {
      try {
        const user = await api.post({ body })
        this.user = user

        localStorage.setItem("authorized", true)

        return true
      } catch (err) {
        console.error(err)
      }
    },
    async logout() {
      try {
        await api.delete()
        localStorage.setItem("authorized", "")
        this.user = null
      } catch(err) {
        console.error(err)
      }
    },
    async getUsers() {
      this.pending = true
      try {
        const users = await uapi.get()
        this.users = users
        this.pending = false
      } catch(err) {
        this.pending = false
        console.error(err)
        return Promise.reject()
      }
    },
    async createUser(body) {
      this.pending = true

      try {
        await uapi.post({ body })
        await this.getUsers()
        this.pending = false
      } catch(err) {
        console.error(err)
        this.pending = false
        return Promise.reject()
      }
    },
    async deleteUser(id) {
      this.pending = true

      try {
        await uapi.delete({ path: `/${id}` })
        await this.getUsers()
        this.pending = false
      } catch(err) {
        this.pending = false
        console.error(err)
        return Promise.reject()
      }
    }
  }
})
