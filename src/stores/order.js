import { defineStore } from "pinia"
import { Instance } from "@/api"
import query from "@/common/query"
import dayjs from "dayjs"

const api = new Instance("admin/orders")

const fromUtc = data => {
	const date = dayjs.utc(data).format("YYYY-MM-DD")
	const time = dayjs.utc(data).format("HH:mm:ss")
	
	return dayjs(`${date}${time}`).toDate()
}

export const useOrderStore = defineStore("order", {
	state: () => ({
		pending: false,
		orders: null,
		total: 0,
		page: 1,
		limit: 15,
		keyword: "",
		date_from: "",
		date_to: ""
	}),
	actions: {
		async getOrders() {
			try {
				this.pending = true

				let path = query({
					page: this.page,
					limit: this.limit,
					keyword: this.keyword,
					date_from: this.date_from,
					date_to: this.date_to
				})
	
				const { orders, total } = await api.get({ path })

				this.orders = orders
				this.total = total
			} catch(err) {
				console.error(err)
			}

			this.pending = false
		},
		async getOrder(id) {
			try {
				const order = await api.get({ path: id })

				order.start = fromUtc(order.start)
				order.end = fromUtc(order.end)

				return order
			} catch(err) {
				console.error(err)
				return Promise.reject()
			}
		},
		async approveOrder(id, body) {
			this.pending = true
			try {
				await api.put({ path: id, body })
			} catch(err) {
				console.error(err)
			}
			this.pending = false
		},
		async createOrder(body) {
			this.pending = true

			try {
				const order = await api.post({ body })
				this.pending = false
				
				return order.id
			} catch(err) {
				console.error(err)
				this.pending = false
				return Promise.reject(err)
			}
		}
	}
})