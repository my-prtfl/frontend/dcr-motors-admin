import { useUserStore } from '@/stores/user'
import { createRouter, createWebHistory } from 'vue-router'

import NProgress from "nprogress"

NProgress.configure({ showSpinner: false })

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  linkActiveClass: "is-active",
  linkExactActiveClass: "is-active",
  routes: [
    {
      path: "/",
      name: "Login",
      component: () => import("@/views/Login.vue")
    },
    {
      path: "/crm",
      component: () => import("@/views/Wrapper.vue"),
      redirect: { name: "Orders" },
      children: [
        {
          path: "orders",
          component: () => import("@/views/Wrapper.vue"),
          redirect: { name: "Orders" },
          children: [
            { path: "", name: "Orders", component: () => import("@/views/crm/Orders.vue") },
            { path: ":orderId", name: "Order", component: () => import("@/views/crm/Order.vue") },
            { path: "create", name: "OrderCreate", component: () => import("@/views/crm/OrderCreate.vue") }
          ]
        },
        {
          path: "clients",
          component: () => import("@/views/Wrapper.vue"),
          redirect: { name: "Clients" },
          children: [
            { path: "", name: "Clients", component: () => import("@/views/crm/Clients.vue") },
            { path: ":clientId", name: "Client", component: () => import("@/views/crm/Client.vue") },
            { path: "create", name: "ClientCreate", component: () => import("@/views/crm/ClientCreate.vue") }
          ]
        }
      ]
    },
    {
      path: "/cars",
      component: () => import("@/views/Wrapper.vue"),
      redirect: { name: "Cars" },
      children: [
        { name: "Cars", path: "", component: () => import("@/views/cars/List.vue") },
        { name: "CarCreate", path: "create", component: () => import("@/views/cars/Create.vue") },
        { name: "Car", path: ":id", component: () => import("@/views/cars/Car.vue") }
      ]
    },
    {
      path: "/brands",
      component: () => import("@/views/Wrapper.vue"),
      redirect: { name: "Brands" },
      children: [
        { name: "Brands", path: "", component: () => import("@/views/brands/List.vue") },
        { name: "BrandCreate", path: "create", component: () => import("@/views/brands/Create.vue") },
        { name: "Brand", path: ":id", component: () => import("@/views/brands/Brand.vue") }
      ]
    },
    {
      path: "/settings",
      component: () => import("@/views/Wrapper.vue"),
      redirect: { name: "RentalParams" },
      children: [
        { path: "meta", name: "RootMetaParams", component: () => import("@/views/settings/Metadata.vue") },
        { path: "rental", name: "RentalParams", component: () => import("@/views/settings/Rental.vue") },
        { path: "images", name: "Images", component: () => import("@/views/settings/Images.vue") }
      ]
    },
    {
      path: "/content",
      component: () => import("@/views/Wrapper.vue"),
      redirect: { name: "PrivacyContent" },
      children: [
        { path: "agreement", name: "PrivacyContent", component: () => import("@/views/content/Privacy.vue") },
        { path: "about-us", name: "AboutUsContent", component: () => import("@/views/content/AboutUs.vue") },
        { path: "faq", name: "FaqContent", component: () => import("@/views/content/Faq.vue") }
      ]
    },
    {
      path: "/crm-settings",
      component: () => import("@/views/Wrapper.vue"),
      redirect: { name: "Users" },
      children: [
        { path: "users", name: "Users", component: () => import("@/views/crm_settings/Users.vue") }
      ]
    }
  ]
})

router.beforeEach(to => {
  if (to.name)
    NProgress.start()
})

router.afterEach(NProgress.done)

router.beforeResolve(async to => {
  const us = useUserStore()
  
  if (localStorage.getItem("authorized")) {
    return await us.authByToken()
      .then(success => {
        // if (success && to.name == "Login")
          // return { name: "Funnel" }

        if (!success && to.name !== "Login")
          return { name: "Login" }
      })
  }

  if (to.name !== "Login")
    return { name: "Login" }

})

export default router
