import { library, dom } from "@fortawesome/fontawesome-svg-core"

import { faAddressCard, faAngleDown, faAnglesRight, faArrowRight, faArrowRightFromBracket, faArrowsUpDownLeftRight, faBars, faCar, faCheckSquare, faCircleExclamation, faCopy, faGear, faGlobe, faImage, faLightbulb, faList, faNewspaper, faPlus, faPlusCircle, faQuestionCircle, faSpinner, faTableColumns, faTrashCan, faUpload, faUser, faUsers, faXmark, faXmarkSquare, faCircleXmark } from "@fortawesome/free-solid-svg-icons"

import { faTelegram, faViber, faWhatsapp } from "@fortawesome/free-brands-svg-icons"
import { faSquare } from "@fortawesome/free-regular-svg-icons"

library.add(
	faTableColumns, 
	faArrowRightFromBracket, 
	faAnglesRight,
	faAngleDown,
	faGear, 
	faPlus,
	faPlusCircle,
	faCircleExclamation,
	faXmark,
	faXmarkSquare,
	faCheckSquare,
	faList,
	faSpinner,
	faTrashCan,
	faArrowsUpDownLeftRight,
	faUpload,
	faArrowRight,
	faCar,
	faBars,
	faNewspaper,
	faAddressCard,
	faQuestionCircle,
	faLightbulb,
	faUser,
	faCopy,
	faUsers,
	faGlobe,
	faTelegram,
	faWhatsapp,
	faViber,
	faImage,
	faSquare,
	faCircleXmark
)

dom.watch()