import axios from "axios"

import router from "@/router"

import { useToast } from 'vue-toastification'

const toast = useToast()

const getI18n = async () => {
	const i18n = (await import("@/i18n")).default
	return i18n
}

export class Instance {
	#api

	constructor(path = "api/admin") {
		this.#api = axios.create({
			baseURL: `//${import.meta.env.VITE_API_URI}/${path}`,
			withCredentials: true
		})
	}

	#req(method, params = { path: "", body: {} }) {
		const { path, body } = params
		return new Promise(( resolve, reject ) => {
			return this.#api[method](path || "", body)
				.then(res => resolve(res.data))
				.catch(async err => {
					const { global: { t } } = await getI18n()

					if (err.response?.status) {
						if (err.response?.data?.message) {
							toast.error(t(`default.err.${err.response.data.message}`))
						} else {
							const { status } = err.response
							
							switch (status) {
								case 401:
									localStorage.setItem("authorized", "")
									router.push({ name: "Login" })
									toast.error(t("default.auth_err"))
									break;
								case 403:
									router.push({ name: "Orders" })
									toast.error(t("default.err.permissions"))
									break;
								case 404:
									toast.error(t("default.not_exist"))
									break;
								default:
									toast.error(t("default.bad"))
									break;
							}
						}
					} else {
						if (err.message == "Network Error")
							toast.error(t("default.network_err"))
						else
							toast.error(t("default.code_err"))
					}

					reject(err)
				})
		})
	}

	get(params) {
		return this.#req("get", params)
	}

	post(params) {
		return this.#req("post", params)
	}

	put(params) {
		return this.#req("put", params)
	}

	delete(params) {
		return this.#req("delete", params)
	}
}