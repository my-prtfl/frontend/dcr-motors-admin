# DCR-Motors admin

> DCR-motors is a Dubai car rental website

This project is an administrative part of [DCR-Motors](https://dcr-motors.com)

Cannot work properly without [api](https://gitlab.com/my-prtfl/backend/dcr-motors-api)

## Stack
* Vite
* Vue 3
* Sass
* [Bulma](https://bulma.io)

## Functional:
* Applications processing
* Adding and editing brands and cars
* Editing [FAQ](https://dcr-motors/en/faq), [About us](https://dcr-motors/en/about-us) and [Privacy policy](https://dcr-motors.com/en/privacy) pages content
